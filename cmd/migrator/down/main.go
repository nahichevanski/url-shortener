package main

import (
	"errors"
	"log"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/sqlite3"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func main() {

	storagePath := "./store/store.db"
	migPath := "./migrations"

	m, err := migrate.New("file://"+migPath, "sqlite3://"+storagePath)
	if err != nil {
		log.Fatalf("migrations error: %s", err)
	}

	if err := m.Down(); err != nil {
		if errors.Is(err, migrate.ErrNoChange) {
			log.Println("no migrations to apply")
			return
		}
		log.Fatalf("migrations up error: %s", err)
		os.Exit(1)
	}

	log.Println("migrations applied successfully")
}
