package main

import (
	"context"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
	"url-shortener/internal/cache/redis"
	"url-shortener/internal/config"
	"url-shortener/internal/lib/slg"
	"url-shortener/internal/server"
	"url-shortener/internal/shortener"
	"url-shortener/internal/storage/sqlite"
)

func main() {

	cfg := config.MustLoad()

	log := setupLogger(cfg.Env)
	log.Debug("init logger", slog.Any("cfg", cfg))

	storage, err := sqlite.New(cfg.Storage)
	if err != nil {
		log.Error("init storage fail", slg.Err(err))
		os.Exit(1)
	}
	log.Debug("storage initialized", slog.Any("storage", storage))

	cache, err := redis.New(cfg)
	if err != nil {
		log.Error("init cache fail", slg.Err(err))
		os.Exit(1)
	}
	log.Debug("cache initialized", slog.Any("cache", cache))

	service := shortener.New(storage, cache, log)

	server := server.New(log, service, cfg)

	go func() {
		err = server.Run()
		if err != nil {
			log.Error("server crashed", slg.Err(err))
		}
	}()

	log.Info("server started", slog.Any("server", server))

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-quit

	err = server.Stop(context.Background())
	if err != nil {
		log.Error("error occured on server shutting down", slg.Err(err))
	}
	log.Info("Server shutting down")

	err = storage.Stop()
	if err != nil {
		log.Error("error occured on db connection close", slg.Err(err))
	}
	log.Info("Database closed")

	err = cache.Stop()
	if err != nil {
		log.Error("error occured on cache connection close", slg.Err(err))
	}
	log.Info("Cache closed")
}

func setupLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case "local":
		log = slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
	case "dev":
		log = slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
	case "prod":
		log = slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}))
	}
	return log
}
