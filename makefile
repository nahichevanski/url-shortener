run: ## Start application
run:
	go run cmd/app/main.go

test: ## Start test with coverage
test:
	go test -v -cover ./...

all-pkg-test: ## Start test with coverage
all-pkg-test:
	go test -v -cover -coverpkg ./... ./...

migrate-up: ## Apply migration
migrate-up: 
	go run cmd/migrator/main.go

migrate-down: ## Apply migration
migrate-down: 
	go run cmd/migrator/down/main.go

run-m-up: ## Start application and migrations up
run-m-up:
	go run cmd/migrator/main.go
	go run cmd/app/main.go