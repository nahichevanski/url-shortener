package redis

import (
	"context"
	"errors"
	"fmt"
	"url-shortener/internal/config"

	"github.com/redis/go-redis/v9"
)

type Cache struct {
	rds *redis.Client
}

func New(cfg *config.Config) (*Cache, error) {
	const oper = "redis.New"

	rds := redis.NewClient(
		&redis.Options{
			Addr:     cfg.Cache.Addr,
			Password: cfg.Cache.Password,
			DB:       0,
		},
	)

	_, err := rds.Ping(context.Background()).Result()
	if err != nil {
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return &Cache{rds: rds}, nil
}

// Set set value
func (c *Cache) Set(key, value string) error {
	const oper = "redis.Set"

	err := c.rds.Set(context.Background(), key, value, 0).Err()
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	return nil
}

// Get get value
func (c *Cache) Get(key string) (string, error) {
	const oper = "redis.Get"

	val, err := c.rds.Get(context.Background(), key).Result()
	if err != nil {
		return "", fmt.Errorf("%s: %w", oper, err)
	}

	return val, nil
}

// Delete delete value
func (c *Cache) Delete(key string) error {
	const oper = "redis.Delete"

	err := c.rds.Del(context.Background(), key).Err()
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}

	return nil
}

// Exists check key
func (c *Cache) Exists(key string) (bool, error) {
	const oper = "redis.Exists"

	isExists, err := c.rds.Exists(context.Background(), key).Result()

	switch {
	case err != nil:
		return false, fmt.Errorf("%s: %w", oper, err)
	case isExists == 0:
		return false, nil
	case isExists == 1:
		return true, nil
	default:
		return false, fmt.Errorf("%s: %w", oper, errors.New("unexpected error"))
	}
}

func (c *Cache) Stop() error {
	return c.rds.Close()
}
