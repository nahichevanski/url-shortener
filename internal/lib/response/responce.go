package response

import (
	"fmt"
	"strings"

	"github.com/go-playground/validator/v10"
)

type Request struct {
	URL   string `json:"url" validate:"required,url"`
	Alias string `json:"alias,omitempty"`
}

type Response struct {
	Status  string `json:"status"`
	Message string `json:"message,omitempty"`
}

const (
	statusOK    = "OK"
	statusError = "error"

	AliasLength = 10
)

func OK() Response {
	return Response{
		Status: statusOK,
	}
}

func Error(msg string) Response {
	return Response{
		Status:  statusError,
		Message: msg,
	}
}

func ValidationError(errs validator.ValidationErrors) Response {
	var errMsgs []string

	for _, err := range errs {
		switch err.ActualTag() {
		case "required":
			errMsgs = append(errMsgs, fmt.Sprintf("field %s is a required field", err.Field()))
		case "url":
			errMsgs = append(errMsgs, fmt.Sprintf("field %s is not a valid URL", err.Field()))
		default:
			errMsgs = append(errMsgs, fmt.Sprintf("field %s is not valid", err.Field()))
		}
	}

	return Response{
		Status:  statusError,
		Message: strings.Join(errMsgs, ", "),
	}
}
