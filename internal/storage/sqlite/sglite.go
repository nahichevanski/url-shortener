package sqlite

import (
	"database/sql"
	"errors"
	"fmt"
	"url-shortener/internal/storage"

	"github.com/mattn/go-sqlite3"
	_ "github.com/mattn/go-sqlite3"
)

type Storage struct {
	db *sql.DB
}

func New(storagePath string) (*Storage, error) {
	const oper = "sqlite.New"

	db, err := sql.Open("sqlite3", storagePath)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return &Storage{
		db: db,
	}, nil
}

// SaveURL saves url and its alias
func (s *Storage) InsertURL(urlForSave, alias string) (int64, error) {
	const oper = "sqlite.SaveURL"

	query := `insert into urls (url, alias) values(?, ?)`

	stmt, err := s.db.Prepare(query)
	if err != nil {
		return 0, fmt.Errorf("%s: %w", oper, err)
	}
	defer stmt.Close()

	res, err := stmt.Exec(urlForSave, alias)
	if err != nil {
		if sqliteErr, ok := err.(sqlite3.Error); ok && sqliteErr.ExtendedCode == sqlite3.ErrConstraintUnique {
			return 0, fmt.Errorf("%s: %w", oper, storage.ErrUrlAlreadyExists)
		}
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return id, nil
}

// SelectURL gets an url by alias
func (s *Storage) SelectURL(alias string) (string, error) {
	const oper = "sqlite.SelectURL"

	query := `select url from urls where alias = ?`

	stmt, err := s.db.Prepare(query)
	if err != nil {
		return "", fmt.Errorf("%s: %w", oper, err)
	}
	defer stmt.Close()

	var url string

	err = stmt.QueryRow(alias).Scan(&url)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return "", storage.ErrUrlNotFound
		}
		return "", fmt.Errorf("%s: %w", oper, err)
	}

	return url, nil
}

// DeleteUrl deletes url by alias
func (s *Storage) DeleteURL(alias string) error {
	const oper = "sqlite.DeleteURL"

	query := `delete from urls where alias = ?`

	stmt, err := s.db.Prepare(query)
	if err != nil {
		return fmt.Errorf("%s: %w", oper, err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(alias)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return storage.ErrUrlNotFound
		}
		return fmt.Errorf("%s: %w", oper, err)
	}
	defer rows.Close()
	return nil
}

// Stop close db connection
func (s *Storage) Stop() error {
	return s.db.Close()
}
