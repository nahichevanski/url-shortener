package storage

import "errors"

var ErrUrlNotFound = errors.New("URL not found")
var ErrUrlAlreadyExists = errors.New("URL already exists")
