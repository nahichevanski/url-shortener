package config

import (
	"log"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Env     string `yaml:"env" env-default:"local" env-required:"true"`
	Storage string `yaml:"storage" env-default:"./store/store.db" env-required:"true"`
	Server  Server `yaml:"server"`
	Cache   Cache  `yaml:"cache"`
}

type Server struct {
	Addr        string        `yaml:"addr" env-default:"localhost:24680"`
	Timeout     time.Duration `yaml:"timeout" env-default:"4s"`
	IdleTimeout time.Duration `yaml:"idle_timeout" env-default:"60s"`
	User        string        `yaml:"user" env-required:"true"`
	Password    string        `yaml:"password" env-required:"true" env:"HTTP_SERVER_PASSWORD"`
}

type Cache struct {
	Addr     string `yaml:"addr" env-default:"localhost:6379"`
	Password string `yaml:"password"`
}

// MustLoad load config, returns pointer on config.Config and panic if something wrong.
func MustLoad() *Config {
	var cfg Config

	path := "./config/config.yaml"

	err := cleanenv.ReadConfig(path, &cfg)
	if err != nil {
		log.Fatalf("read config error: %s", err)
	}

	return &cfg
}
