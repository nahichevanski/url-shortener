package redirect

import (
	"errors"
	"log/slog"
	"net/http"
	"unicode"

	resp "url-shortener/internal/lib/response"
	"url-shortener/internal/lib/slg"
	"url-shortener/internal/storage"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

//go:generate mockgen -source=./redirect.go -destination=./testredirect/redirect.go --package=testredirect
type UrlRedirecter interface {
	RedirectUrl(alias string) (string, error)
}

func OnUrl(log *slog.Logger, ur UrlRedirecter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		const oper = "redirect.OnUrl"

		log := log.With(slog.String("operation", oper))

		alias := chi.URLParam(r, "alias")

		if alias == "" {
			log.Error("alias not specified")
			w.WriteHeader(http.StatusBadRequest)
			render.JSON(w, r, resp.Error("alias not specified"))
			return
		}

		for _, ch := range alias {
			if unicode.IsSpace(ch) {
				log.Error("alias contains a space")
				w.WriteHeader(http.StatusBadRequest)
				render.JSON(w, r, resp.Error("alias must not contain a space"))
				return
			}
		}

		log.Info("alias received", slog.String("alias", alias))

		url, err := ur.RedirectUrl(alias)
		if err != nil {

			if errors.Is(err, storage.ErrUrlNotFound) {
				log.Error("non-existent alias has been entered", slg.Err(err))
				w.WriteHeader(http.StatusBadRequest)
				render.JSON(w, r, resp.Error("non-existent alias"))
				return
			}

			log.Error("failed to get url", slg.Err(err))
			w.WriteHeader(http.StatusInternalServerError)
			render.JSON(w, r, resp.Error("failed to get url"))
			return
		}

		log.Info("url received from db", slog.String("url", url))

		http.Redirect(w, r, url, http.StatusFound)
	}
}
