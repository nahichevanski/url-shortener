package remove

import (
	"errors"
	"log/slog"
	"net/http"
	"unicode"

	resp "url-shortener/internal/lib/response"
	"url-shortener/internal/lib/slg"
	"url-shortener/internal/storage"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
)

//go:generate mockgen -source=./remove.go -destination=./testremove/remove.go --package=testremove
type UrlRemover interface {
	RemoveUrl(alias string) error
}

func Url(log *slog.Logger, ur UrlRemover) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		const oper = "remove.Url"

		log := log.With(
			slog.String("operation", oper),
			slog.String("request_id", middleware.GetReqID(r.Context())),
		)

		alias := r.URL.Query().Get("alias")

		if alias == "" {
			log.Error("alias not specified")
			w.WriteHeader(http.StatusBadRequest)
			render.JSON(w, r, resp.Error("alias not specified"))
			return
		}

		for _, ch := range alias {
			if unicode.IsSpace(ch) {
				log.Error("alias contains a space")
				w.WriteHeader(http.StatusBadRequest)
				render.JSON(w, r, resp.Error("alias must not contain a space"))
				return
			}
		}

		log.Info("alias received", slog.String("alias", alias))

		err := ur.RemoveUrl(alias)
		if err != nil {

			if errors.Is(err, storage.ErrUrlNotFound) {
				log.Error("non-existent alias has been entered", slg.Err(err))
				w.WriteHeader(http.StatusBadRequest)
				render.JSON(w, r, resp.Error("non-existent alias"))
				return
			}

			log.Error("failed to delete url", slg.Err(err))
			w.WriteHeader(http.StatusInternalServerError)
			render.JSON(w, r, resp.Error("failed to delete url"))
			return
		}

		log.Info("url deleted from db by alias", slog.String("alias", alias))

		render.JSON(w, r, resp.OK())
	}
}
