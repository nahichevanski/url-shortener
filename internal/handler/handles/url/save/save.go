package save

import (
	"errors"
	"io"
	"log/slog"
	"net/http"
	"unicode"

	"url-shortener/internal/lib/random"
	resp "url-shortener/internal/lib/response"
	"url-shortener/internal/lib/slg"
	"url-shortener/internal/storage"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
	"github.com/go-playground/validator/v10"
)

//go:generate mockgen -source=./save.go -destination=./testsave/save.go --package=testsave
type UrlSaver interface {
	SaveURL(urlForSave, alias string) (int64, error)
}

type Response struct {
	Response resp.Response `json:"response"`
	Alias    string        `json:"alias,omitempty"`
}

func Url(log *slog.Logger, urlSaver UrlSaver) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		const oper = "save.Url"

		log := log.With(
			slog.String("operation", oper),
			slog.String("request_id", middleware.GetReqID(r.Context())),
		)

		var req resp.Request

		err := render.DecodeJSON(r.Body, &req)
		if err != nil {

			if errors.Is(err, io.EOF) {
				log.Error("request body is empty")
				w.WriteHeader(http.StatusBadRequest)
				render.JSON(w, r, resp.Error("empty request"))
				return
			}

			log.Error("failed to decode request body", slg.Err(err))
			w.WriteHeader(http.StatusInternalServerError)
			render.JSON(w, r, resp.Error("failed to decode request"))
			return
		}

		for _, ch := range req.Alias {
			if unicode.IsSpace(ch) {
				log.Error("alias contains a space")
				w.WriteHeader(http.StatusBadRequest)
				render.JSON(w, r, resp.Error("alias must not contain a space"))
				return
			}
		}

		log.Info("request body decoded", slog.Any("request", req))

		if err := validator.New().Struct(req); err != nil {
			validateErr := err.(validator.ValidationErrors)
			log.Error("invalid request", slg.Err(err))
			w.WriteHeader(http.StatusBadRequest)
			render.JSON(w, r, resp.ValidationError(validateErr))
			return
		}

		alias := req.Alias
		if alias == "" {
			alias = random.Alias(resp.AliasLength)
		}

		id, err := urlSaver.SaveURL(req.URL, alias)
		if errors.Is(err, storage.ErrUrlAlreadyExists) {
			log.Info("url already exists", slog.String("url", req.URL))
			w.WriteHeader(http.StatusBadRequest)
			render.JSON(w, r, resp.Error("url already exists"))
			return
		}
		if err != nil {
			log.Error("failed to add url", slg.Err(err))
			w.WriteHeader(http.StatusInternalServerError)
			render.JSON(w, r, resp.Error("failed to add url"))
			return
		}

		log.Info("url added", slog.Int64("id", id))

		render.JSON(w, r, Response{
			Response: resp.OK(),
			Alias:    alias,
		})
	}
}
