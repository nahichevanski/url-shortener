package get

import (
	"errors"
	"log/slog"
	"net/http"
	"unicode"

	resp "url-shortener/internal/lib/response"
	"url-shortener/internal/lib/slg"
	"url-shortener/internal/storage"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
)

//go:generate mockgen -source=./get.go -destination=./testhandler/get.go --package=testhandler
type UrlGetter interface {
	GetUrl(alias string) (string, error)
}

type Response struct {
	Response resp.Response `json:"response"`
	Url      string        `json:"url,omitempty"`
}

func Url(log *slog.Logger, ug UrlGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		const oper = "get.Url"

		log := log.With(
			slog.String("operation", oper),
			slog.String("request_id", middleware.GetReqID(r.Context())),
		)

		alias := r.URL.Query().Get("alias")

		if alias == "" {
			log.Error("alias not specified")
			w.WriteHeader(http.StatusBadRequest)
			render.JSON(w, r, resp.Error("alias not specified"))
			return
		}

		for _, ch := range alias {
			if unicode.IsSpace(ch) {
				log.Error("alias contains a space")
				w.WriteHeader(http.StatusBadRequest)
				render.JSON(w, r, resp.Error("alias must not contain a space"))
				return
			}
		}

		log.Info("alias received", slog.String("alias", alias))

		url, err := ug.GetUrl(alias)
		if err != nil {

			if errors.Is(err, storage.ErrUrlNotFound) {
				log.Error("non-existent alias has been entered", slg.Err(err))
				w.WriteHeader(http.StatusBadRequest)
				render.JSON(w, r, resp.Error("non-existent alias"))
				return
			}

			log.Error("failed to get url", slg.Err(err))
			w.WriteHeader(http.StatusInternalServerError)
			render.JSON(w, r, resp.Error("failed to get url"))
			return
		}

		log.Info("url received from db", slog.String("url", url))

		render.JSON(w, r, Response{
			Response: resp.OK(),
			Url:      url,
		})
	}
}
