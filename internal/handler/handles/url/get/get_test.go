package get_test

import (
	"encoding/json"
	"errors"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"url-shortener/internal/handler/handles/url/get"
	"url-shortener/internal/handler/handles/url/get/testhandler"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestUrl(t *testing.T) {
	t.Parallel()
	cases := []struct {
		name    string
		alias   string
		url     string
		message string
		status  int
		mockErr error
	}{
		{
			name:   "Success",
			alias:  "test_alias",
			url:    "https://google.com",
			status: http.StatusOK,
		},
		{
			name:    "Space In Alias",
			alias:   " 2",
			url:     "",
			message: "no call",
			status:  http.StatusBadRequest,
		},
		{
			name:    "Empty Alias",
			alias:   "",
			url:     "",
			message: "no call",
			status:  http.StatusBadRequest,
		},
		{
			name:    "Non-exists Alias",
			alias:   "test_alias",
			url:     "",
			message: "some error",
			status:  http.StatusInternalServerError,
		},
	}

	for _, tc := range cases {

		t.Run(tc.name, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			ug := testhandler.NewMockUrlGetter(ctrl)

			if tc.message == "" {
				ug.EXPECT().GetUrl(tc.alias).Return(tc.url, nil)
			}

			if tc.message != "" && tc.message != "no call" {
				ug.EXPECT().GetUrl(tc.alias).Return("", errors.New("test error"))
			}

			log := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

			handler := get.Url(log, ug)

			req, err := http.NewRequest(http.MethodPost, "/url?alias="+tc.alias, nil)
			require.Nil(t, err)

			rr := httptest.NewRecorder()
			handler.ServeHTTP(rr, req)

			require.Equal(t, rr.Code, tc.status)

			body := rr.Body.String()

			var resp get.Response

			require.NoError(t, json.Unmarshal([]byte(body), &resp))

			if tc.message == "" {
				require.Equal(t, tc.message, resp.Response.Message)
			} else {
				require.Equal(t, tc.url, resp.Url)
			}

		})
	}
}
