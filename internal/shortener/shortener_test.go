package shortener_test

import (
	"errors"
	"log/slog"
	"os"
	"testing"

	"url-shortener/internal/shortener"
	"url-shortener/internal/shortener/testdata"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestSaveUrl(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name  string
		url   string
		alias string
		id    int64
		err   error
	}{
		{
			name:  "Success",
			url:   "https://google.com",
			alias: "google",
			id:    1,
			err:   nil,
		},
		{
			name:  "Empty Url",
			url:   "",
			alias: "google",
			id:    0,
			err:   errors.New("some error"),
		},
		{
			name:  "Empty Alias",
			url:   "https://google.com",
			alias: "",
			id:    0,
			err:   errors.New("some error"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			log := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

			mockStorage := testdata.NewMockStorage(ctrl)

			mockShort := shortener.New(mockStorage, log)

			require.NotNil(t, mockShort)

			mockStorage.EXPECT().InsertURL(tc.url, tc.alias).Return(tc.id, tc.err)

			id, err := mockShort.SaveURL(tc.url, tc.alias)

			if tc.err != nil {
				require.NotNil(t, err)
				require.Equal(t, tc.id, id)
			} else {
				require.Nil(t, err)
				require.Equal(t, tc.id, id)
			}
		})
	}
}

func TestGetUrl(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name  string
		url   string
		alias string
		err   error
	}{
		{
			name:  "Success",
			url:   "https://google.com",
			alias: "google",
			err:   nil,
		},
		{
			name:  "Space Alias",
			alias: " ",
			err:   errors.New("some error"),
		},
		{
			name:  "Empty Alias",
			alias: "",
			err:   errors.New("some error"),
		},
		{
			name:  "Non-exists Alias",
			alias: "#$5--",
			err:   errors.New("some error"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			log := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

			mockStorage := testdata.NewMockStorage(ctrl)

			mockShort := shortener.New(mockStorage, log)

			require.NotNil(t, mockShort)

			mockStorage.EXPECT().SelectURL(tc.alias).Return(tc.url, tc.err)

			url, err := mockShort.GetUrl(tc.alias)

			if tc.err != nil {
				require.NotNil(t, err)
			} else {
				require.Nil(t, err)
				require.Equal(t, tc.url, url)
			}
		})
	}
}

func TestRemoveUrl(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name  string
		alias string
		err   error
	}{
		{
			name:  "Success",
			alias: "google",
			err:   nil,
		},
		{
			name:  "Space Alias",
			alias: " ",
			err:   errors.New("some error"),
		},
		{
			name:  "Empty Alias",
			alias: "",
			err:   errors.New("some error"),
		},
		{
			name:  "Non-exists Alias",
			alias: "#$5--",
			err:   errors.New("some error"),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			log := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

			mockStorage := testdata.NewMockStorage(ctrl)

			mockShort := shortener.New(mockStorage, log)

			require.NotNil(t, mockShort)

			mockStorage.EXPECT().DeleteURL(tc.alias).Return(tc.err)

			err := mockShort.RemoveUrl(tc.alias)

			require.Equal(t, tc.err, err)

		})
	}
}
