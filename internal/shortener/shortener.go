package shortener

import (
	"log/slog"
	"url-shortener/internal/lib/slg"
)

//go:generate mockgen -source=./shortener.go -destination=./testdata/shortener.go --package=testdata
type Storage interface {
	InsertURL(urlForSave, alias string) (int64, error)
	SelectURL(alias string) (string, error)
	DeleteURL(alias string) error
}

type Cache interface {
	Set(key, value string) error
	Get(key string) (string, error)
	Delete(key string) error
	Exists(key string) (bool, error)
}

type Shortener struct {
	storage Storage
	cache   Cache
	log     *slog.Logger
}

// New recieves Storage(data layer) and returns Shortener(service layer)
func New(s Storage, c Cache, log *slog.Logger) *Shortener {
	return &Shortener{
		storage: s,
		cache:   c,
		log:     log,
	}
}

// Save call SaveURL from Storage
func (s *Shortener) SaveURL(urlForSave, alias string) (int64, error) {
	const oper = "cases.Save"

	id, err := s.storage.InsertURL(urlForSave, alias)
	if err != nil {
		s.log.Error(oper, slg.Err(err))
		return 0, err
	}

	err = s.cache.Set(alias, urlForSave)
	if err != nil {
		s.log.Error(oper, slg.Err(err))
	}

	return id, nil
}

// GetUrl call SelectURL from storage or getting from Cache if exists, if not exists - put in cache.
func (s *Shortener) GetUrl(alias string) (string, error) {
	const oper = "cases.GetUrl"

	isExists, err := s.cache.Exists(alias)
	if err != nil {
		s.log.Error(oper, slg.Err(err))
	}

	if isExists {
		urlFromCache, err := s.cache.Get(alias)
		if err != nil {

			s.log.Error(oper, slg.Err(err))

			urlFromDB, err := s.storage.SelectURL(alias)
			if err != nil {
				s.log.Error(oper, slg.Err(err))
				return "", err
			}

			s.log.Debug("url getting from DB", slog.String("operation", oper))

			err = s.cache.Set(alias, urlFromDB)
			if err != nil {
				s.log.Error(oper, slg.Err(err))
			}
			return urlFromDB, nil
		}
		s.log.Debug("url getting from cache", slog.String("operation", oper))
		return urlFromCache, nil
	}

	url, err := s.storage.SelectURL(alias)
	if err != nil {
		s.log.Error(oper, slg.Err(err))
		return "", err
	}

	err = s.cache.Set(alias, url)
	if err != nil {
		s.log.Error(oper, slg.Err(err))
	}

	s.log.Debug("url getting from DB", slog.String("operation", oper))
	return url, nil
}

// RemoveUrl call DeleteURL from storage
func (s *Shortener) RemoveUrl(alias string) error {
	const oper = "cases.RemoveUrl"

	err := s.storage.DeleteURL(alias)
	if err != nil {
		s.log.Error(oper, slg.Err(err))
		return err
	}

	err = s.cache.Delete(alias)
	if err != nil {
		s.log.Error(oper, slg.Err(err))
	}

	return nil
}

// RedirectUrl call SelectURL from storage
func (s *Shortener) RedirectUrl(alias string) (string, error) {
	const oper = "cases.RedirectUrl"

	url, err := s.storage.SelectURL(alias)
	if err != nil {
		s.log.Error(oper, slg.Err(err))
		return "", err
	}
	return url, nil
}
