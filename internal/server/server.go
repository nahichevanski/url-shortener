package server

import (
	"context"
	"log/slog"
	"net/http"
	"url-shortener/internal/config"
	"url-shortener/internal/handler/handles/redirect"
	"url-shortener/internal/handler/handles/url/get"
	"url-shortener/internal/handler/handles/url/remove"
	"url-shortener/internal/handler/handles/url/save"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type Service interface {
	save.UrlSaver
	get.UrlGetter
	remove.UrlRemover
	redirect.UrlRedirecter
}

type Server struct {
	httpSrv *http.Server
}

func New(log *slog.Logger, srv Service, cfg *config.Config) *Server {

	httpSrv := &http.Server{
		Addr:         cfg.Server.Addr,
		ReadTimeout:  cfg.Server.Timeout,
		WriteTimeout: cfg.Server.Timeout,
		IdleTimeout:  cfg.Server.IdleTimeout,
		Handler:      initRoutes(log, srv, cfg),
	}

	return &Server{
		httpSrv: httpSrv,
	}
}

func (s *Server) Run() error {
	return s.httpSrv.ListenAndServe()
}

func (s *Server) Stop(ctx context.Context) error {
	return s.httpSrv.Shutdown(ctx)
}

func initRoutes(log *slog.Logger, srv Service, cfg *config.Config) http.Handler {
	router := chi.NewRouter()

	router.Use(middleware.RequestID)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(middleware.URLFormat)

	router.Route("/url", func(r chi.Router) {

		r.Use(middleware.BasicAuth("url-shortener", map[string]string{
			cfg.Server.User: cfg.Server.Password,
		}))

		r.Post("/", save.Url(log, srv))
		r.Delete("/{alias}", remove.Url(log, srv))
		r.Get("/", get.Url(log, srv))
		r.Get("/{alias}", redirect.OnUrl(log, srv))
	})

	return router
}
